package com.bigfans.framework.plugins;

import java.io.File;
import java.io.InputStream;

import com.bigfans.framework.Applications;
import com.bigfans.framework.utils.FileUtils;
import com.bigfans.framework.utils.IOUtils;
import com.bigfans.framework.utils.WebUtils;
import com.bigfans.framework.web.RequestHolder;
import org.springframework.util.ResourceUtils;

import javax.servlet.http.HttpServletRequest;


/**
 * 
 * @Title: 
 * @Description: 本地存储
 * @author lichong 
 * @date 2015年12月23日 上午9:33:07 
 * @version V1.0
 */
public class LocalStoragePlugin implements FileStoragePlugin {

	public void plugin() {
		
	}

	public void unplugin() {
		
	}
	
	public String getStorageType() {
		return "local";
	}

	@Override
	public boolean fileExists(String path) {
		return false;
	}

	public UploadResult listFiles(String path) {
		return null;
	}

	public UploadResult upload(File srcFile, String targetPath) {
		HttpServletRequest request = RequestHolder.getHttpServletRequest();
		String targetFolder = WebUtils.getWebDiskPath() + "static/images/";
		File destFile = new File(targetFolder, targetPath);
		FileUtils.moveTo(srcFile, destFile);
		String domain = WebUtils.getDomain(request);
		UploadResult res = new UploadResult();
		res.setFilePath(domain + "/images" + targetPath);
		res.setStorageType(getStorageType());
		res.setServer(domain);
		res.setFileKey(targetPath);
		return res;
	}
	
	public File download(String targetPath) {
		String targetFolder = WebUtils.getWebDiskPath() + "static/images/";
		File destFile = new File(targetFolder, targetPath);
		return destFile;
	}

	public UploadResult uploadQrCodeImg(File srcFile, String orderNo) {
		// TODO Auto-generated method stub
		return null;
	}

	public UploadResult uploadProductImg(File srcFile, String prodId) {
		// TODO Auto-generated method stub
		return null;
	}

	public UploadResult uploadFile(InputStream is ,String dest) {
		String tempForder = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		File target = new File(tempForder, dest);
		if(!target.exists()){
			target.mkdirs();
		}
		IOUtils.copyToFile(is, target);
		UploadResult res = new UploadResult();
		res.setFilePath(target.getPath());
		res.setStorageType(getStorageType());
		return res;
	}

	public UploadResult deleteFile(String target) {
		String targetFolder = WebUtils.getWebDiskPath() + "static/images/";
		File file = new File(targetFolder , target);
		file.delete();
		UploadResult res = new UploadResult();
		res.setSuccess(true);
		return res;
	}

}
