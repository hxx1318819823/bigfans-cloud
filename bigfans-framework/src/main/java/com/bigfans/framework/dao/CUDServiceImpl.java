package com.bigfans.framework.dao;

import java.util.List;

import com.bigfans.framework.exception.ServiceRuntimeException;
import com.bigfans.framework.model.AbstractModel;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2015年8月25日 下午3:04:41 
 * @version V1.0
 */
public abstract class CUDServiceImpl {

	public <M extends AbstractModel> int batchCreate(List<M> eList) throws ServiceRuntimeException {
		return new BeanDecorator(eList).batchInsert();
	}

	public <M extends AbstractModel> void create(M e) throws ServiceRuntimeException {
		new BeanDecorator(e).insert();
	}

	public <M extends AbstractModel> int delete(M e) throws ServiceRuntimeException {
		return new BeanDecorator(e).delete();
	}

	public int delete(String[] ids) throws ServiceRuntimeException {
		return 0;
	}
	
	public <M extends AbstractModel> int delete(Class<M> clazz , String id) throws ServiceRuntimeException {
		M e;
		try {
			e = clazz.newInstance();
		} catch (Exception ex) {
			throw new ServiceRuntimeException(ex);
		}
		e.setId(id);
		return new BeanDecorator(e).delete();
	}

	public <M extends AbstractModel> int update(M e) throws ServiceRuntimeException {
		return new BeanDecorator(e).update();
	}
	
	public <M extends AbstractModel> Long count(M e) throws ServiceRuntimeException {
		return new BeanDecorator(e).count();
	}
}
