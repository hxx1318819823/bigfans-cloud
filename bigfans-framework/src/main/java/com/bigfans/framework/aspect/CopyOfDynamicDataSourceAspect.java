package com.bigfans.framework.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.transaction.annotation.Transactional;

import com.bigfans.framework.dao.DynamicDataSourceHolder;

@Aspect
@Order(-1)
public class CopyOfDynamicDataSourceAspect {
	
	private static final String CLUSTER = "@annotation(clusterDataSource)";
	private static final String MASTER = "@annotation(masterDataSource)";
	
	@Before(MASTER)
	public void changeMasterDataSource (JoinPoint call, Transactional transactional) {
		DynamicDataSourceHolder.write();
	}
	
	@Before(CLUSTER)
	public void changeClusterDataSource (JoinPoint call, Transactional transactional) {
		DynamicDataSourceHolder.read();
	}
	
	@After(CLUSTER)
	public void restoreClusterDataSource(JoinPoint call, Transactional transactional) {
		DynamicDataSourceHolder.clearDataSource();
	}
	
	@After(MASTER)
	public void restoreMasterDataSource(JoinPoint call, Transactional transactional) {
		DynamicDataSourceHolder.clearDataSource();
	}
	
}
