package com.bigfans.framework.es.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;

/**
 * 
 * @Description:
 * @author lichong 
 * 2015年2月9日上午10:09:02
 *
 */
public class SearchCriteria extends AbstractSearchCriteria implements Serializable{
	
	private static final long serialVersionUID = 996295877727619834L;
	
	private QueryBuilder query;
	private QueryBuilder queryFilter;
	private QueryBuilder filter;
	private List<AggregationBuilder> aggregations;
	private HighlightBuilder.Field[] highlightFields;
	
	public SearchCriteria(QueryBuilder query) {
		this.query = query;
	}

	public SearchCriteria(QueryBuilder query, QueryBuilder filter) {
		this.query = query;
		this.filter = filter;
	}

	public SearchCriteria(QueryBuilder query, QueryBuilder filter, List<FieldSortBuilder> sorts) {
		this.query = query;
		this.filter = filter;
		this.sortList = sorts;
	}
	
	public SearchCriteria(QueryBuilder query, QueryBuilder filter, List<FieldSortBuilder> sorts, List<AggregationBuilder> aggregations) {
		this.query = query;
		this.filter = filter;
		this.sortList = sorts;
		this.aggregations = aggregations;
	}

	public SearchCriteria(QueryBuilder query, QueryBuilder filter, List<FieldSortBuilder> sorts, List<AggregationBuilder> aggregations ,HighlightBuilder.Field[] highlightFields) {
		this.query = query;
		this.filter = filter;
		this.sortList = sorts;
		this.aggregations = aggregations;
		this.highlightFields = highlightFields;
	}
	
	public QueryBuilder getQuery() {
		return query;
	}

	public QueryBuilder getFilter() {
		return filter;
	}

	public HighlightBuilder.Field[] getHighlightFields() {
		return highlightFields;
	}

	public List<AggregationBuilder> getAggregations() {
		return aggregations;
	}


	public void addAggregation(AggregationBuilder aggregationBuilder) {
		if (aggregations == null) {
			aggregations = new ArrayList<AggregationBuilder>();
		}
		aggregations.add(aggregationBuilder);
	}

	public void setAggregations(List<AggregationBuilder> aggregations) {
		this.aggregations = aggregations;
	}

	public QueryBuilder getQueryFilter() {
		return queryFilter;
	}

	public void setQueryFilter(QueryBuilder queryFilter) {
		this.queryFilter = queryFilter;
	}
	
}
