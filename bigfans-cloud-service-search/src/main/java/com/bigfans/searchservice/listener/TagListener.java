package com.bigfans.searchservice.listener;

import com.bigfans.framework.es.ElasticTemplate;
import com.bigfans.framework.es.IndexDocument;
import com.bigfans.framework.es.request.CreateDocumentCriteria;
import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.tag.TagCreatedEvent;
import com.bigfans.searchservice.api.clients.CatalogServiceClient;
import com.bigfans.searchservice.model.Tag;
import com.bigfans.searchservice.schema.mapping.TagMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-03-18 下午5:30
 **/
@Component
@KafkaConsumerBean
public class TagListener {

    @Autowired
    private CatalogServiceClient catalogServiceClient;
    @Autowired
    private ElasticTemplate elasticTemplate;

    @KafkaListener
    public void on(TagCreatedEvent event) throws Exception {
        IndexDocument doc = new IndexDocument(event.getId());
        doc.put("id", event.getId());
        doc.put("name", event.getName());
        doc.put("related_count", event.getRelatedCount());

        CreateDocumentCriteria createDocumentCriteria = new CreateDocumentCriteria();
        createDocumentCriteria.setIndex(TagMapping.INDEX);
        createDocumentCriteria.setType(TagMapping.TYPE);
        createDocumentCriteria.setDocument(doc);
        elasticTemplate.insert(createDocumentCriteria);
    }

}
