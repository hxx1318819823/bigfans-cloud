package com.bigfans.searchservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.framework.dao.ParameterMap;
import com.bigfans.searchservice.dao.CategoryDAO;
import com.bigfans.searchservice.model.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(CategoryDAOImpl.BEAN_NAME)
public class CategoryDAOImpl extends MybatisDAOImpl<Category> implements CategoryDAO {

	public static final String BEAN_NAME = "categoryDAO";
	
	@Override
	public String getParentId(String catId) {
		ParameterMap params = new ParameterMap();
		params.put("catId", catId);
		return getSqlSession().selectOne(className + ".getParentId", params);
	}

	public List<Category> listByLevel(Integer level , String parentId , boolean showInNav) {
		ParameterMap params = new ParameterMap();
		params.put("level", level);
		params.put("parentId", parentId);
		params.put("showInNav", showInNav);
		return getSqlSession().selectList(className + ".list", params);
	}

}
