package com.bigfans.catalogservice.api.mgr;

import java.util.List;

import com.bigfans.catalogservice.model.SpecOption;
import com.bigfans.catalogservice.service.spec.SpecOptionService;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.model.PageContext;
import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class SpecManageApi {

    @Autowired
    private SpecOptionService optionService;

    @GetMapping(value = "/specs")
    @NeedLogin(roles = {"operator" , "admin"})
    public RestResponse list(
            @RequestParam(value = "catId", required = false) String catId,
            @RequestParam(value = "prodId", required = false) String prodId,
            @RequestParam(value = "cp", required = false, defaultValue = "1") Long cp,
            @RequestParam(value = "ps", required = false, defaultValue = "20") Long ps
    ) throws Exception {
        PageContext.setCurrentPage(cp);
        PageContext.setPageSize(ps);
        List<SpecOption> specOptions = null;
        if (StringHelper.isNotEmpty(catId)) {
            specOptions = optionService.listByCatId(catId, PageContext.getStart(), PageContext.getPageSize());
        } else if (StringHelper.isNotEmpty(prodId)) {
            specOptions = optionService.listByPid(prodId, PageContext.getStart(), PageContext.getPageSize());
        } else {
            specOptions = optionService.list(catId, null, prodId, PageContext.getStart(), PageContext.getPageSize());
        }
        return RestResponse.ok(specOptions);
    }


    @GetMapping(value = "/specs/{id}")
    @NeedLogin(roles = {"operator" , "admin"})
    public RestResponse detail(
            @PathVariable(value = "id") String specId
    ) throws Exception {
        SpecOption specOption = optionService.load(specId);
        return RestResponse.ok(specOption);
    }

    @PostMapping(value = "/spec")
    @NeedLogin(roles = {"operator" , "admin"})
    public RestResponse create(@RequestBody SpecOption specOption) throws Exception {
        if (specOption.getInputType().equals(SpecOption.INPUTTYPE_LIST)) {
            String values = CollectionUtils.join(specOption.getValueList(), ",");
            specOption.setValues(values);
        }
        optionService.create(specOption);
        return RestResponse.ok();
    }

    @PostMapping(value = "/spec/{id}")
    @NeedLogin(roles = {"operator" , "admin"})
    public RestResponse update(@RequestBody SpecOption specOption , @PathVariable(value = "id") String id) throws Exception {
        if (specOption.getInputType().equals(SpecOption.INPUTTYPE_LIST)) {
            String values = CollectionUtils.join(specOption.getValueList(), ",");
            specOption.setValues(values);
        }
        specOption.setId(id);
        optionService.update(specOption);
        return RestResponse.ok();
    }

    @DeleteMapping(value = "/spec/{id}")
    @NeedLogin(roles = {"operator" , "admin"})
    public RestResponse delete(@PathVariable(value = "id") String id) throws Exception {
        optionService.delete(id);
        return RestResponse.ok();
    }
}
