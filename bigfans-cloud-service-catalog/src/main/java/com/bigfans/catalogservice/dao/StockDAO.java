package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.Stock;
import com.bigfans.framework.dao.BaseDAO;

/**
 * 
 * @Description:
 * @author lichong 2015年5月31日下午9:11:57
 *
 */
public interface StockDAO extends BaseDAO<Stock> {

    Integer stockOut(String prodId, Integer quantity);

    Integer stockIn(String prodId, Integer quantity);

    Integer lockStock(String prodId, Integer quantity);

    Integer releaseStock(String prodId, Integer quantity);

    Stock getByProd(String prodId);

    Stock getBySkuValKey(String valKey);
}
