package com.bigfans.catalogservice.model.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 商品规格组,封装商品规格项->规格值
 * 
 * @author lichong
 *
 */
@Data
public class SpecGroupVO implements Serializable{

	private static final long serialVersionUID = 7399149219218538867L;
	
	protected String option;
	protected List<SpecValueVO> values;

	public void addValue(SpecValueVO valueVO){
		if(this.values == null){
			this.values = new ArrayList<>();
		}
		this.values.add(valueVO);
	}
}
