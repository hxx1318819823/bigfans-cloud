package com.bigfans.catalogservice.service.attribute;

import com.bigfans.catalogservice.dao.AttributeOptionDAO;
import com.bigfans.catalogservice.model.AttributeOption;
import com.bigfans.catalogservice.model.AttributeValue;
import com.bigfans.catalogservice.model.Category;
import com.bigfans.catalogservice.service.category.CategoryService;
import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.event.ApplicationEventBus;
import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.utils.CollectionUtils;
import com.bigfans.framework.utils.StringHelper;
import com.bigfans.model.event.attribute.AttributeCreatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lichong
 * @version V1.0
 * @Title:
 * @Description:
 * @date 2015年10月7日 上午11:52:42
 */
@Service(AttributeOptionServiceImpl.BEAN_NAME)
public class AttributeOptionServiceImpl extends BaseServiceImpl<AttributeOption> implements
        AttributeOptionService {

    public static final String BEAN_NAME = "attributeOptionService";

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttributeValueService attributeValueService;
    @Autowired
    private ApplicationEventBus eventBus;

    private AttributeOptionDAO attributeOptionDAO;

    @Autowired
    public AttributeOptionServiceImpl(AttributeOptionDAO attributeOptionDAO) {
        super(attributeOptionDAO);
        this.attributeOptionDAO = attributeOptionDAO;
    }

    @Override
    @Transactional
    public void create(AttributeOption e) throws Exception {
        super.create(e);
        if (e.getInputType().equals(AttributeOption.INPUTTYPE_LIST)) {
            List<AttributeValue> valueList = new ArrayList<>();
            List<String> values = e.getValues();
            for (String value : values) {
                AttributeValue attributeValue = new AttributeValue();
                attributeValue.setOptionId(e.getId());
                attributeValue.setOptionName(e.getName());
                attributeValue.setCategoryId(e.getCategoryId());
                attributeValue.setValue(value);
                valueList.add(attributeValue);
            }
            attributeValueService.batchCreate(valueList);
        }
        AttributeCreatedEvent event = new AttributeCreatedEvent(e.getId());
        eventBus.publishEvent(event);
    }

    @Override
    @Transactional
    public int update(AttributeOption e) throws Exception {
        return super.update(e);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AttributeOption> listByCategory(String catId) throws Exception {
        List<String> catIds = null;
        if (StringHelper.isNotEmpty(catId)) {
            catIds = categoryService.listParentIds(catId);
            catIds.add(catId);
        }
        List<AttributeOption> options = attributeOptionDAO.listByCategory(catIds);
        for (AttributeOption option : options) {
            List<AttributeValue> valueList = attributeValueService.listByOptionId(option.getId());
            option.setValueList(valueList);
        }
        return options;
    }
}
