package com.bigfans.paymentservice.model;

import com.bigfans.paymentservice.model.entity.PayMethodEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @Description:支付类型
 * @author lichong 2015年4月4日下午9:22:44
 *
 */
public class PayMethod extends PayMethodEntity {

	private static final long serialVersionUID = -8769137454888850975L;
	
	private List<PayMethod> subMethods = new ArrayList<PayMethod>();

	public List<PayMethod> getSubMethods() {
		return subMethods;
	}

	public void setSubMethods(List<PayMethod> subMethods) {
		this.subMethods = subMethods;
	}
}
