package com.bigfans.userservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.userservice.model.UserPointLog;
import com.bigfans.userservice.service.UserPointLogService;
import org.springframework.stereotype.Service;

@Service(UserPointLogServiceImpl.BEAN_NAME)
public class UserPointLogServiceImpl extends BaseServiceImpl<UserPointLog> implements UserPointLogService {

    public static final String BEAN_NAME = "userPointLogServiceImpl";

}
