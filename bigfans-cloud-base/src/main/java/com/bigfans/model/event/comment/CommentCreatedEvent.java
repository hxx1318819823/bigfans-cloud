package com.bigfans.model.event.comment;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

@Data
public class CommentCreatedEvent extends AbstractEvent{

    private String orderItemId;
    private String commentId;

    public CommentCreatedEvent(String orderItemId , String commentId) {
        this.orderItemId = orderItemId;
        this.commentId = commentId;
    }
}
