package com.bigfans.shippingservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.shippingservice.dao.DeliveryDAO;
import com.bigfans.shippingservice.model.Delivery;
import com.bigfans.shippingservice.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @Description:配送服务类
 * @author lichong
 * 2014年12月16日上午10:13:56
 *
 */
@Service(DeliveryServiceImpl.BEAN_NAME)
public class DeliveryServiceImpl extends BaseServiceImpl<Delivery> implements DeliveryService {

	public static final String BEAN_NAME = "deliveryService";
	
	@Autowired
	public DeliveryServiceImpl(DeliveryDAO deliveryDAO) {
		super(deliveryDAO);
	}
}
