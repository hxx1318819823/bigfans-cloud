package com.bigfans.systemservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.utils.MD5;
import com.bigfans.systemservice.dao.OperatorDAO;
import com.bigfans.systemservice.model.Operator;
import com.bigfans.systemservice.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户服务类
 * @author lichong
 *
 */
@Service(OperatorServiceImpl.BEAN_NAME)
public class OperatorServiceImpl extends BaseServiceImpl<Operator> implements OperatorService {
	
	public static final String BEAN_NAME = "userService";
	
	private OperatorDAO operatorDAO;

	@Autowired
	private OperatorService _this;
	
	@Autowired
	public OperatorServiceImpl(OperatorDAO operatorDAO) {
		super(operatorDAO);
		this.operatorDAO = operatorDAO;
	}

	@Transactional
	public Operator register(Operator user) throws Exception{
		user.setPassword(MD5.toMD5(user.getPassword()));
		super.create(user);
		return user;
	}

	@Transactional(readOnly=true)
	@Override
	public Operator login(String account, String password) throws Exception {
		return login(account , password , true);
	}
	
	@Override
	public Operator login(String username, String password, boolean md5) throws Exception {
		Operator example = new Operator();
		example.setUsername(username);
		if(md5){
			example.setPassword(MD5.toMD5(password));
		}else{
			example.setPassword(password);
		}
		Operator loginUser = load(example);
		return loginUser;
	}
	
	@Transactional(readOnly=true)
	@Override
	public boolean emailExist(String email) throws Exception {
		Operator userEntity = new Operator();
		userEntity.setEmail(email);
		Long count = super.count(userEntity);
		return count > 0;
	}
	
	@Transactional(readOnly=true)
	@Override
	public boolean accountExist(String username) throws Exception {
		Operator userEntity = new Operator();
		userEntity.setUsername(username);
		Long count = count(userEntity);
		return count > 0;
	}
}
