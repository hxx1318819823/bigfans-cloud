package com.bigfans.orderservice.ordertracker;

/**
 * @author lichong
 * @create 2018-04-17 上午7:35
 **/
public class ExpiredOrderTrackerThread implements Runnable {

    public ExpiredOrderTracker tracker;

    @Override
    public void run() {
        tracker = new ExpiredOrderTracker();
        tracker.startTrack();
    }
}
