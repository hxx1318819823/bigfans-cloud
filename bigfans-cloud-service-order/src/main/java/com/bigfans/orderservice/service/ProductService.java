package com.bigfans.orderservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.orderservice.model.Product;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月4日下午9:29:21
 *
 */
public interface ProductService extends BaseService<Product> {

}
