package com.bigfans.orderservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.orderservice.model.PayMethod;

import java.util.List;


/**
 * 
 * @Description:
 * @author lichong
 * 2015年4月4日下午9:29:21
 *
 */
public interface PayMethodService extends BaseService<PayMethod> {

	List<PayMethod> listOnlinePayMethods() throws Exception;
	
	List<PayMethod> listTopMethods() throws Exception;
	
	PayMethod getByCode(String code) throws Exception;
	
}
