package com.bigfans.orderservice.model;

import com.bigfans.model.dto.order.OrderItemPromotionDto;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lichong
 * @create 2018-02-17 下午5:53
 **/
@Data
public class OrderCheckoutItem {

    private String prodId;
    /* 产品名称 */
    private String prodName;
    /* 产品图片 */
    private String prodImg;

    private Integer quantity;

    private BigDecimal price;

    private BigDecimal originalSubTotal;
    private BigDecimal subTotal;
    private BigDecimal promotionDeduction;
    private List<OrderItemPromotionDto> promotions = new ArrayList<>();
    private List<OrderItemSpec> specs = new ArrayList<>();

    public void addSpec(OrderItemSpec spec){
        this.specs.add(spec);
    }
}
